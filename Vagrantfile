# -*- mode: ruby -*-
# vi: set ft=ruby :

NODE_COUNT = 2
NETWORK = '192.168.2.'
FIRST_IP = 50
MASTER_NODE = 'MasterNode'

nodes = []
(0..NODE_COUNT-1).each do |i|
  ip = NETWORK + (FIRST_IP + i).to_s
  nodes << ip
end

IO.write("deploy/cassandra_hosts.conf", "hosts=[" + nodes.map{ |n| '"' + n + '"' }.join(",") + "]")

servers = []
(0..NODE_COUNT-1).each do |i|
  if (i == NODE_COUNT-1)
    name = MASTER_NODE
  else
    name = 'Node' + i.to_s
  end
  ip = nodes[i]
  servers << {'name' => name, 'ip' => ip}
end

install_basic_packages = <<-SCRIPT
  echo "Installing basic packages..."
  apt-get install curl -y

  apt-get update
  apt-get install -y software-properties-common python-software-properties

  apt-get install -y apt-transport-https ca-certificates  
SCRIPT

add_vagrant_keys = <<-SCRIPT
  echo "Installing vagrant keys..."
  curl -s https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub > ~/.ssh/id_rsa.pub
  curl -s https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant > ~/.ssh/id_rsa
  chmod 600 ~/.ssh/id_rsa
SCRIPT

install_sbt = <<-SCRIPT
  echo "Installing SBT..."
  echo "deb https://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list
  apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 642AC823
  apt-get update
  apt-get install -y sbt
SCRIPT

adjust_timezone = <<-SCRIPT
  echo "Adjusting TimeZone..."
  echo "Europe/Moscow" | tee /etc/timezone
  dpkg-reconfigure -f noninteractive tzdata
SCRIPT

install_jdk = <<-SCRIPT
  echo "Installing JDK..."
  echo "deb http://httpredir.debian.org/debian jessie-backports main" | tee -a /etc/apt/sources.list
  apt-get update
  apt-get -y upgrade
  apt-get -f -y install
  
  JTYPE=$1
  
  if [ "$JTYPE" == "JRE" ]; then
    apt-get -t jessie-backports install -y openjdk-8-jre
  else
    apt-get -t jessie-backports install -y openjdk-8-jdk
  fi	
SCRIPT

install_cassandra = <<-SCRIPT
  echo "Installing Cassandra..."
  echo "deb http://www.apache.org/dist/cassandra/debian 311x main" | tee -a /etc/apt/sources.list.d/cassandra.sources.list
  curl https://www.apache.org/dist/cassandra/KEYS | apt-key add -
  apt-get update
  apt-get install -y cassandra
  apt-get install -y ntp

  service cassandra stop
  rm -rf /var/lib/cassandra/*
  cp /home/vagrant/LocationTrackerWS/deploy/cassandra.yaml /etc/cassandra/ -v
  
  ADDRESS=$1
  RAWNODES=$2
  NODES=$(echo "$RAWNODES" | sed 's/[][]//g' | sed 's/"//g')
  
  echo "Address: $ADDRESS"
  echo "Nodes: $NODES"
    
  cd /etc/cassandra/
  sed -i -e 's/__CLUSTER_NAME__/LocationTrackerCluster/g' cassandra.yaml
  sed -i -e "s/__SEEDS__/$NODES/g" cassandra.yaml
  sed -i -e "s/__LISTEN_ADDRESS__/$ADDRESS/g" cassandra.yaml
  sed -i -e "s/__RPC_ADDRESS__/$ADDRESS/g" cassandra.yaml

  service cassandra start
SCRIPT

master_node_run = <<-SCRIPT
  echo "Master Node run..."
  echo "Check if Cassandra is ready..."
  ADDRESS=$1
  if ! nc -z $ADDRESS 9042; then
    sudo service cassandra stop
    sudo service cassandra start
    echo "Waiting for Cassandra to start..."
    while ! nc -z $ADDRESS 9042; do
       sleep 1
    done
    echo "Cassandra is ready."
  fi

  sudo service cassandra status
  nodetool status
  
  cd /home/vagrant/LocationTrackerWS
  cp /home/vagrant/LocationTrackerWS/deploy/cassandra_hosts.conf /home/vagrant/LocationTrackerWS/conf -v
  sbt test
  #sbt run
SCRIPT

Vagrant.configure(2) do |config|
  config.vm.box = "debian/jessie64"
  config.ssh.insert_key = false

  servers.each do |server|
    config.vm.define server['name'] do |lt|
      lt.vm.provider "virtualbox" do |vb|
        vb.gui = false
        # Customize the amount of memory on the VM:
        vb.memory = "4096"
      end

      lt.vm.hostname = server['name']
      lt.vm.network "private_network", ip: server['ip']

      lt.vm.network "forwarded_port", guest: 9000, host: 9000, auto_correct: true

      lt.vm.provision "file", run: "always", source: "./", destination: "/home/vagrant/"

      lt.vm.provision "shell", run: "once", privileged: true,  inline: install_basic_packages
      lt.vm.provision "shell", run: "once", privileged: false, inline: add_vagrant_keys
      lt.vm.provision "shell", run: "once", privileged: true,  inline: adjust_timezone

      if (server['name'] == MASTER_NODE)
        lt.vm.provision "shell", run: "once", privileged: true,  inline: install_jdk, args: "JDK"
      else
        lt.vm.provision "shell", run: "once", privileged: true,  inline: install_jdk, args: "JRE"
      end

      lt.vm.provision "shell", run: "once", privileged: true,  inline: install_cassandra, args: ["#{server['ip']}", "#{nodes}"]
	  
      if (server['name'] == MASTER_NODE)
        lt.vm.provision "shell", run: "once", privileged: true,  inline: install_sbt
        lt.vm.provision "shell", run: "always", privileged: false, inline: master_node_run, args: "#{server['ip']}"
      end
    end
  end
end
