package model.utils

import model.LocationRecord

object LocationComponents extends Enumeration {
  val latitude:  Value = Value("latitude")
  val longitude: Value = Value("longitude")
}

object LocationRecordUtils {
  val MinLat: Double = -89
  val MaxLat: Double =  90
  val MinLon: Double = -179
  val MaxLon: Double =  180

  def validateCoordinateRange(locationRecord: LocationRecord): Set[LocationComponents.Value] = {

    def validateRange(value: Double, min: Double, max: Double, component: LocationComponents.Value, acc: Set[LocationComponents.Value]): Set[LocationComponents.Value] = {
      if ((value < min) || (value > max))
        acc + component
      else
        acc
    }

    validateRange(locationRecord.lon, MinLon, MaxLon, LocationComponents.longitude,
    validateRange(locationRecord.lat, MinLat, MaxLat, LocationComponents.latitude, Set.empty))
  }
}
