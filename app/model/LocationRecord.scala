package model

import java.util.UUID

case class LocationRecord(
                           id: UUID,
                           sourceId: UUID,
                           lat: Double,
                           lon: Double
                         )