package utils

import java.util.concurrent.TimeUnit
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

object Implicits {

  trait TimeoutProvider {
    def timeoutDuration: Int
  }

  implicit class ImplicitFuture[C](future: Future[C]) {

    private def result(timeoutDuration: Int): C = Await.result(future, Duration(timeoutDuration, TimeUnit.SECONDS))

    def mapResult[U](timeoutDuration: Int)(mapResultFunc: C => U): U = mapResultFunc(result(timeoutDuration))

    def mapResult[U](mapResultFunc: C => U)(implicit timeoutProvider: TimeoutProvider): U = mapResult(timeoutProvider.timeoutDuration)(mapResultFunc)
  }
}