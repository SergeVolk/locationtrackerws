package controllers

import java.util.UUID

import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.mvc._

import scala.collection.mutable.MutableList
import utils.Implicits._
import repository._
import model._
import model.utils.{LocationComponents, LocationRecordUtils}
import infrastructure.BasicSettings

trait LocationControllerInterface {
  def getLocationsBySourceId(sourceId: String): Action[AnyContent]
  def createLocationRecord: Action[AnyContent]
  def deleteLocationsBySourceId(sourceId: String): Action[AnyContent]
}

trait LocationControllerComponent extends Controller with LocationControllerInterface {

  self: LocationRepositoryComponent with BasicSettings =>

  private implicit object timeoutProvider extends TimeoutProvider {
    def timeoutDuration: Int = self.timeoutDuration
  }

  private implicit val locationRecordReader: Reads[LocationRecord] = //Json.reads[CoordinateRecord]
  (
    (JsPath \ "id").read[UUID] and
    (JsPath \ "sourceId").read[UUID] and
    (JsPath \ "lat").read[Double] and
    (JsPath \ "lon").read[Double]
  ) (LocationRecord)

  private implicit val locationRecordWriter: Writes[LocationRecord] = //Json.writes[CoordinateRecord]
  (
    (JsPath \ "id").write[UUID] and
    (JsPath \ "sourceId").write[UUID] and
    (JsPath \ "lat").write[Double] and
    (JsPath \ "lon").write[Double]
  ) (unlift(LocationRecord.unapply))

  private implicit val validationErrorWriter: Writes[ValidationErrorDescription] = Json.writes[ValidationErrorDescription]

  private case class ValidationErrorDescription(path: String, errorDescription: String)

  private def customValidation(locationRecord: LocationRecord): Option[Seq[ValidationErrorDescription]] = {

    val errors = new MutableList[ValidationErrorDescription]()

    val coordRangeErrors = LocationRecordUtils.validateCoordinateRange(locationRecord)
    if (coordRangeErrors.contains(LocationComponents.latitude)) {
      errors += ValidationErrorDescription(
        "lat",
        s"""Latitude must be between [${LocationRecordUtils.MinLat}; ${LocationRecordUtils.MaxLat}]. """ +
        s"""Current value is ${locationRecord.lat}.""")
    }

    if (coordRangeErrors.contains(LocationComponents.longitude)) {
      errors += ValidationErrorDescription(
        "lon",
        s"""Longitude must be between [${LocationRecordUtils.MinLon}; ${LocationRecordUtils.MaxLon}]. """ +
        s"""Current value is ${locationRecord.lon}.""")
    }

    if (errors.nonEmpty) {
      Some(errors)
    }
    else {
      None
    }
  }

  private def checkAndInsertRecord(rec: LocationRecord): Result = {
    customValidation(rec) match {
      case None => {
        repository
          .insertLocationRecord(rec)
          .mapResult(res => {
            if (res.isSuccessful)
              Created(Json.toJson(rec))
            else
              NotAcceptable(Json.toJson(s"Cannot insert record '$rec' to repository."))
          })
      }
      case Some(errors) => NotAcceptable(Json.toJson(errors))
    }
  }

  private def processRequestBody(jsonBlock: JsValue): Result = {
    Json.fromJson[LocationRecord](jsonBlock) match {
      case JsSuccess(rec, _) => checkAndInsertRecord(rec)
      case JsError(errors)   => {
        val errorDesc = errors.flatMap { e =>
          e._2.map(ve =>
            ValidationErrorDescription(e._1.toJsonString, ve.message)
          )
        }
        NotAcceptable(Json.toJson(errorDesc))
      }
    }
  }

  private def NoRecordsFoundResult = NotFound("No records with such source id found.")

  def getLocationsBySourceId(sourceId: String): Action[AnyContent] = Action {
    repository
      .findLocationsBySourceId(UUID.fromString(sourceId))
      .mapResult(records => {
        if (records.nonEmpty) {
          Ok(Json.toJson(records))
        }
        else {
          NoRecordsFoundResult
        }}
      )
  }

  def createLocationRecord: Action[AnyContent] = Action {
    request => {
      val jsonBody = request.body.asJson
      jsonBody match {
        case Some(jsonBlock) => processRequestBody(jsonBlock)
        case None => NotAcceptable
      }
    }
  }

  def deleteLocationsBySourceId(sourceId: String): Action[AnyContent] = Action {
    repository
      .deleteLocationsBySourceId(UUID.fromString(sourceId))
      .mapResult(res => {
        if (res.isSuccessful) {
          Ok(sourceId)
        }
        else {
          NoRecordsFoundResult
        }
      })
  }
}

