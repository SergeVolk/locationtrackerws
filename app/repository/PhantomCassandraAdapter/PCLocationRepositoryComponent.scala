package repository.PhantomCassandraAdapter

import java.util.UUID
import com.outworkers.phantom.dsl.ResultSet
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import model.LocationRecord
import repository.LocationRepositoryComponent
import repository.PhantomCassandraInterface.PCLocationDatabaseProvider

trait PCLocationRepositoryComponent extends LocationRepositoryComponent {

  this: PCLocationDatabaseProvider =>

  override def repository: LocationRepository = repo

  private lazy val repo = new CassandraLocationRepository

  private class CassandraLocationRepository extends LocationRepository {

    def insertLocationRecord(coordinateRecord: LocationRecord): Future[InsertResult] = {
      val insertDuture = database.insertOrUpdate(coordinateRecord)
      insertDuture.map(resultSet => new InsertResult {
        def isSuccessful: Boolean = resultSet.wasApplied
      })
    }

    def findLocationsBySourceId(sourceId: UUID): Future[List[LocationRecord]] =
       database.getBySourceId(sourceId)

    def deleteLocationsBySourceId(sourceId: UUID): Future[DeleteResult] = {

      val delFuture: Future[ResultSet] = database.deleteBySourceId(sourceId)
      delFuture.map(resultSet => new DeleteResult {
        def isSuccessful: Boolean = resultSet.wasApplied
      })
    }
  }
}
