package repository.PhantomCassandraAdapter

import java.util.UUID
import java.util.concurrent.TimeUnit
import scala.concurrent.duration._
import scala.concurrent.Future

import infrastructure.BasicSettings
import model.LocationRecord
import repository.PhantomCassandraInterface.PCLocationDatabaseProvider

trait PCDatabaseUtilsComponent {

  this: PCLocationDatabaseProvider with BasicSettings =>

  def dropDB(): Unit = {
    database.dropDB(Duration(timeoutDuration, TimeUnit.SECONDS))
  }

  def createDB(): Unit = {
    database.createDB(Duration(timeoutDuration, TimeUnit.SECONDS))
  }

  def getBySourceId(sourceId: UUID): Future[List[LocationRecord]] = database.getBySourceId(sourceId)
}
