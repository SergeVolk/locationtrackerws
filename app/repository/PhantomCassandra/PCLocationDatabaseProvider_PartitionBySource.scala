package repository.PhantomCassandra

import java.util.UUID
import com.outworkers.phantom.dsl.{Database, ResultSet}
import com.outworkers.phantom.connectors.{CassandraConnection, ContactPoints}
import com.outworkers.phantom.dsl._

import scala.concurrent.Future
import model.LocationRecord
import repository.PhantomCassandraInterface.{PCLocationDatabase, PCLocationDatabaseProvider}

import scala.concurrent.duration.Duration

trait PCLocationDatabaseProvider_PartitionBySource extends PCLocationDatabaseProvider {

  this: {
    val hosts: List[String]
    val keyspace: String
  } =>

  override def database: PCLocationDatabase = db

  private lazy val db: PCLocationDatabase = new LocationDatabaseImpl(connection)
  private lazy val connection: CassandraConnection = ContactPoints(hosts).keySpace(keyspace)

  private class LocationDatabaseImpl(override val connector: CassandraConnection) extends Database[LocationDatabaseImpl](connector) with PCLocationDatabase {

    object CoordinatesBySourceModel extends LocationBySourceModel with connector.Connector

    def insertOrUpdate(locationRecord: LocationRecord): Future[ResultSet] = {
      Batch.logged
        .add(CoordinatesBySourceModel.store(locationRecord))
        .future()
    }

//    def delete(locationRecord: LocationRecord): Future[ResultSet] = {
//      Batch.logged
//        .add(CoordinatesBySourceModel.delete.where(_.sourceId eqs locationRecord.sourceId))
//        .future()
//    }

    def deleteBySourceId(sourceId: UUID): Future[ResultSet] = CoordinatesBySourceModel.deleteBySourceId(sourceId)
    def getBySourceId(sourceId: UUID): Future[List[LocationRecord]] = CoordinatesBySourceModel.getBySourceId(sourceId)

    def createDB(timeout: Duration): Seq[ResultSet] = this.create(timeout)
    def dropDB(timeout: Duration): Seq[ResultSet] = this.drop(timeout)
  }

  abstract class LocationBySourceModel extends Table[LocationBySourceModel, LocationRecord] {

    override def tableName: String = "locations"

    object id extends TimeUUIDColumn with ClusteringOrder {
      override lazy val name = "loc_id"
    }

    object sourceId extends UUIDColumn with PartitionKey
    object lat extends DoubleColumn
    object lon extends DoubleColumn

    def getBySourceId(sourceId: UUID): Future[List[LocationRecord]] = {
      select
        .where(_.sourceId eqs sourceId)
        .consistencyLevel_=(ConsistencyLevel.ONE)
        .fetch()
    }

    def deleteBySourceId(sourceId: UUID): Future[ResultSet] = {
      delete
        .where(_.sourceId eqs sourceId)
        .consistencyLevel_=(ConsistencyLevel.ONE)
        .future()
    }
  }
}
