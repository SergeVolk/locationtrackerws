package repository

import java.util.UUID
import scala.concurrent.Future
import model.LocationRecord

trait LocationRepositoryComponent {

  def repository: LocationRepository

  trait LocationRepository {
    // Queries
    def findLocationsBySourceId(sourceId: UUID): Future[List[LocationRecord]]

    // Commands
    def insertLocationRecord(coordinateRecord: LocationRecord): Future[InsertResult]
    def deleteLocationsBySourceId(sourceId: UUID): Future[DeleteResult]
  }

  trait RepositoryOperationResult {
    def isSuccessful: Boolean
  }

  trait DeleteResult extends RepositoryOperationResult

  trait InsertResult extends RepositoryOperationResult
}