package repository.PhantomCassandraInterface

import java.util.UUID
import com.outworkers.phantom.dsl.ResultSet
import model.LocationRecord
import scala.concurrent.Future
import scala.concurrent.duration.Duration

trait PCLocationDatabase {
  // Data access
  def insertOrUpdate(locationRecord: LocationRecord): Future[ResultSet]
  def getBySourceId(sourceId: UUID): Future[List[LocationRecord]]
  def deleteBySourceId(sourceId: UUID): Future[ResultSet]

  // Management
  def createDB(timeout: Duration): Seq[ResultSet]
  def dropDB(timeout: Duration): Seq[ResultSet]
}

trait PCLocationDatabaseProvider {

  def database: PCLocationDatabase
}