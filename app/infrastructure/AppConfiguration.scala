package infrastructure

import com.typesafe.config.ConfigFactory
import scala.collection.JavaConverters._
import java.io.File

trait BasicSettings {
  val timeoutDuration: Int
}

trait DbSettings {
  val hosts: List[String]
  val keyspace: String

  protected def getDeployHosts: List[String] = {
    val deployConfig = ConfigFactory.parseFile(new File(s"conf${File.separator}cassandra_hosts.conf"))
    if (deployConfig.hasPath("hosts"))
      deployConfig.getStringList("hosts").asScala.toList
    else
      List.empty
  }
  
  protected def getDefaultHosts: List[String] = List("127.0.0.1")
}

trait AppConfiguration extends BasicSettings with DbSettings

trait ProductionAppConfiguration extends AppConfiguration {
  private val config = ConfigFactory.load

  private def getAppConfigHosts: List[String] = {
    if (config.hasPath("cassandra.hosts"))
      config.getStringList("cassandra.hosts").asScala.toList
    else
      List.empty
  }

  override val hosts: List[String] = {

    val appConfigHosts = getAppConfigHosts
    if (appConfigHosts.isEmpty) {
      val deployHosts = getDeployHosts
      if (deployHosts.isEmpty)
        getDefaultHosts
      else
        deployHosts
    }
    else
      appConfigHosts
  }

  override val keyspace: String = config.getString("cassandra.keyspace")
  override val timeoutDuration: Int = 5
}
