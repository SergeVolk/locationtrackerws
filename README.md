# Location Tracking Web Service

## About

This software is a simple REST service allowing clients to report their GPS locations and to retrieve them back by a request.

## Some technical notes

This web-service:

- is written in Scala;
- uses Play! API for request/response management;
- uses Cassandra cluster for distributed storing reported locations;
- is tested using ScalaTest+ engine

## Installation & Running

### Local

- Prerequisites 

    - JDK 1.7+
    - SBT
    - Unoccupied port 9000
    - Cassandra 3.9+

- Launching the API
    
    - Ensure that a Cassandra cluster is installed on this PC and properly configured
    - Check the parameter "cassandra.hosts" in conf/application.conf to point to the Cassandra nodes.
      By default, it should be empty that means that one of Cassandra nodes installed on this PC.
      It is also equivalent to hosts=["localhost"]       
    - sbt run
    - point your REST client onto http://localhost:9000/

- Running the tests

    - Ensure that a Cassandra cluster is installed on this PC and properly configured
    - sbt test

### Inside VM

- Prerequisites

    - Oracle virtualbox 5.0+
    - Vagrant (compatible with your virtualbox version)
    - Rsync
    - SSH
    - Port 9000 free
    - By default, Vagrantfile is configured to run 2 VMs (with Cassandra nodes) each having 4GB of memory.
      So either be sure your PC can provide 8GB of memory or modify the parameter "vb.memory" according to your requirements. 

- Running the tests and API
    
    - vagrant up
    - point your REST client onto http://localhost:9000/