name := "LocationTrackerWS"

version := "0.1"

scalaVersion := "2.11.7"

lazy val LocationTrackerWS = (project in file(".")).enablePlugins(PlayScala)

libraryDependencies ++= Seq(
  "com.github.nscala-time" %% "nscala-time" % "2.12.0",
  "org.scalatestplus" % "play_2.11" % "1.4.0" % Test,
  "org.mockito" % "mockito-core" % "1.9.5" % Test,
  "org.scalamock" %% "scalamock-scalatest-support" % "3.2.2" % Test,
  "com.outworkers"  %%  "phantom-dsl"       % "2.14.5"
)
