import java.util.UUID
import org.scalamock.scalatest.MockFactory
import org.scalatestplus.play._
import play.api.test.Helpers._
import play.api.test._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import controllers.LocationControllerComponent
import repository.LocationRepositoryComponent
import model._

class LocationControllerSpec extends PlaySpec with OneAppPerTest with MockFactory {

  "Location Controller" should {

    val sourceId1 = UUID.fromString("11111111-1111-1111-1111-111111111111")
    val record11 = LocationRecord(UUID.fromString("e66c4dd0-0437-11e8-8cf6-839bb062ce48"), sourceId1, 10, 10)
    val record11Json = """{"id":"e66c4dd0-0437-11e8-8cf6-839bb062ce48","sourceId":"11111111-1111-1111-1111-111111111111","lat":10,"lon":10}"""
    val record12 = LocationRecord(UUID.fromString("e66c4dd1-0437-11e8-8cf6-839bb062ce48"), sourceId1, 20, 20)
    val list1 = List(record11, record12)
    val list1Json =
      """[{"id":"e66c4dd0-0437-11e8-8cf6-839bb062ce48","sourceId":"11111111-1111-1111-1111-111111111111","lat":10,"lon":10},""" +
       """{"id":"e66c4dd1-0437-11e8-8cf6-839bb062ce48","sourceId":"11111111-1111-1111-1111-111111111111","lat":20,"lon":20}]"""

    val sourceId2 = UUID.fromString("22222222-2222-2222-2222-222222222222")

    "return correct json result based on object provided by the Location Controller" in {

      object controller extends LocationControllerComponent with LocationRepositoryComponent with TestAppConfiguration {

        private val repoMock = mock[LocationRepository]
        (repoMock.findLocationsBySourceId _).expects(sourceId1).returning(Future { list1 }).once
        override val repository: LocationRepository = repoMock
      }

      val resultForNonExistingId = call(controller.getLocationsBySourceId(sourceId1.toString), FakeRequest())
      status(resultForNonExistingId) mustBe OK
      contentAsString(resultForNonExistingId) mustBe list1Json
    }

    "create a location record on a valid request" in {

      object controller extends LocationControllerComponent with LocationRepositoryComponent with TestAppConfiguration {

        private val successfulResultSet = mock[InsertResult]
        (successfulResultSet.isSuccessful _).expects().returning(true).anyNumberOfTimes

        private val repoMock = mock[LocationRepository]
        (repoMock.insertLocationRecord _).expects(record11).returning(Future { successfulResultSet }).once
        override val repository: LocationRepository = repoMock
      }

      val creationCallResult = call(controller.createLocationRecord,
        FakeRequest(
          Helpers.POST,
          controllers.routes.LocationController.createLocationRecord().url,
          FakeHeaders(Seq("Content-type" -> "application/json")),
          record11Json))
        status(creationCallResult) mustBe CREATED
        contentAsString(creationCallResult) mustBe record11Json
    }

    "delete existing record" in {

      object controller extends LocationControllerComponent with LocationRepositoryComponent with TestAppConfiguration {

        private val successfulResultSet = mock[DeleteResult]
        (successfulResultSet.isSuccessful _).expects().returning(true).anyNumberOfTimes

        private val repoMock = mock[LocationRepository]
        (repoMock.deleteLocationsBySourceId _).expects(sourceId1).returning(Future { successfulResultSet }).once
        override val repository: LocationRepository = repoMock
      }

      val deleteCallResult = call(controller.deleteLocationsBySourceId(sourceId1.toString),
        FakeRequest(
          Helpers.DELETE,
          controllers.routes.LocationController.deleteLocationsBySourceId(sourceId1.toString).url))

      status(deleteCallResult) mustBe OK
      contentAsString(deleteCallResult) mustBe sourceId1.toString
    }

    "return Not Found on deleting non-existing record" in {

      object controller extends LocationControllerComponent with LocationRepositoryComponent with TestAppConfiguration {

        private val failedResultSet = mock[DeleteResult]
        (failedResultSet.isSuccessful _).expects().returning(false).anyNumberOfTimes

        private val repoMock = mock[LocationRepository]
        (repoMock.deleteLocationsBySourceId _).expects(sourceId2).returning(Future { failedResultSet }).once
        override val repository: LocationRepository = repoMock
      }

      val failedUpdateCallResult = call(controller.deleteLocationsBySourceId(sourceId2.toString),
        FakeRequest(
          Helpers.DELETE,
          controllers.routes.LocationController.deleteLocationsBySourceId(sourceId2.toString).url))

      status(failedUpdateCallResult) mustBe NOT_FOUND
      contentAsString(failedUpdateCallResult) mustBe "No records with such source id found."
    }

    "send 404 on a bad request" in {
      route(app, FakeRequest(GET, "/badReq")).map(status) mustBe Some(NOT_FOUND)
    }

    "fail with HTTP NotAcceptable or nonparseable or empty post and provide validation errors" in {

      object controller extends LocationControllerComponent with LocationRepositoryComponent with TestAppConfiguration {
        override val repository: LocationRepository = mock[LocationRepository]
      }

      status(call(controller.createLocationRecord, FakeRequest())) mustBe NOT_ACCEPTABLE
      val invalidRequest = call(controller.createLocationRecord,
        FakeRequest(
          Helpers.POST,
          controllers.routes.LocationController.createLocationRecord().url,
          FakeHeaders(Seq("Content-type" -> "application/json")), """ {"notId": "_"} """))

      status(invalidRequest) mustBe NOT_ACCEPTABLE
      contentAsString(invalidRequest) mustBe
        """[{"path":"obj.lat","errorDescription":"error.path.missing"},""" +
         """{"path":"obj.sourceId","errorDescription":"error.path.missing"},""" +
         """{"path":"obj.lon","errorDescription":"error.path.missing"},""" +
         """{"path":"obj.id","errorDescription":"error.path.missing"}]"""
    }

    "validate an location record having broken fields" in {

      object controller extends LocationControllerComponent with LocationRepositoryComponent with TestAppConfiguration {
        override val repository: LocationRepository = mock[LocationRepository]
      }

      val invalidRecordJsonCoordOutOfRange = """{"id":"e66c4dd0-0437-11e8-8cf6-839bb062ce48","sourceId":"11111111-1111-1111-1111-111111111111","lat":-90,"lon":-180}"""

      val creationCallResult = call(controller.createLocationRecord,
        FakeRequest(
          Helpers.POST,
          controllers.routes.LocationController.createLocationRecord().url,
          FakeHeaders(Seq("Content-type" -> "application/json")),
          invalidRecordJsonCoordOutOfRange))
      status(creationCallResult) mustBe NOT_ACCEPTABLE
      contentAsString(creationCallResult) mustBe
        """[{"path":"lat","errorDescription":"Latitude must be between [-89.0; 90.0]. Current value is -90.0."},""" +
         """{"path":"lon","errorDescription":"Longitude must be between [-179.0; 180.0]. Current value is -180.0."}]"""
    }
  }
}
