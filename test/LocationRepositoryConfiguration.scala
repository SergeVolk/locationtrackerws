import repository.PhantomCassandra.PCLocationDatabaseProvider_PartitionBySource
import repository.PhantomCassandraAdapter.{PCDatabaseUtilsComponent, PCLocationRepositoryComponent}
import repository.PhantomCassandraInterface.{PCLocationDatabase, PCLocationDatabaseProvider}

object LocationRepositoryConfiguration {

  trait TestDbComp extends PCLocationDatabaseProvider {
    override def database: PCLocationDatabase = testDbComponentSingleton.database
  }

  private object testDbComponentSingleton extends PCLocationDatabaseProvider_PartitionBySource with TestAppConfiguration

  object cassandraUtils extends PCDatabaseUtilsComponent with TestDbComp with TestAppConfiguration

  object repoComp extends PCLocationRepositoryComponent with TestDbComp with TestAppConfiguration
}