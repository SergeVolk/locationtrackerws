import java.util.UUID

import com.datastax.driver.core.utils.UUIDs
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.BeforeAndAfter
import org.scalatestplus.play.PlaySpec

import scala.collection.parallel.immutable.ParSeq
import scala.util.Random
import model.LocationRecord
import utils.Implicits._
import LocationRepositoryConfiguration._

class LocationRepositorySpec extends PlaySpec with BeforeAndAfter with ScalaFutures {

  before {
    cassandraUtils.createDB()
  }

  after {
    cassandraUtils.dropDB()
  }

  private implicit object timeoutProvider extends TimeoutProvider with TestAppConfiguration

  private def getUUID = UUIDs.timeBased()

  private def createCoordinateRecord(sourceId: UUID, lat: Double, lon: Double) = LocationRecord(getUUID, sourceId, lat, lon)

  private def generateCoordinateRecords(sourceId: UUID, count: Int) = {

    (1 to count).map(i => {
      createCoordinateRecord(sourceId, 180 * Random.nextDouble() - 90, 360 * Random.nextDouble() - 180)
    }).toList
  }

  private def generateCoordinateRecordsPar(sourceCount: Int, recordsPerSource: Int): ParSeq[(UUID, List[LocationRecord])] = {

    (1 to sourceCount).par
      .map(_ => UUID.randomUUID)
      .map(s => (s, generateCoordinateRecords(s, recordsPerSource)))
  }

  private def storeRecords(records: ParSeq[(UUID, List[LocationRecord])]): Unit = {

    val storeFutures = records
      .flatMap(_._2)
      .map(s => repoComp.repository.insertLocationRecord(s))
      .seq

    storeFutures.foreach {
      future => future.mapResult(_.isSuccessful mustBe true)
    }
  }

  "Location Repository" should {

    "insert coordinates from a source and return the same coordinates for the source" in {

      val source1 = UUID.randomUUID
      val records = generateCoordinateRecords(source1, 10)

      records.foreach(r => {
        repoComp.repository.insertLocationRecord(r)
          .mapResult(_.isSuccessful mustBe true)
      })

      repoComp.repository.findLocationsBySourceId(source1)
        .mapResult(_ must contain theSameElementsAs records)

      cassandraUtils.getBySourceId(source1)
        .mapResult(_ must contain theSameElementsAs records)
    }

    "insert coordinates from many sources and return the coordinates according to source id" in {

      val parRecords = generateCoordinateRecordsPar(10, 100)

      storeRecords(parRecords)

      parRecords.foreach {
        case (source, records) => {
          cassandraUtils.getBySourceId(source)
            .mapResult(_ must contain theSameElementsAs records)
        }
      }
    }

    "update on insert a record with existing id" in {

      val rec1 = createCoordinateRecord(UUID.randomUUID, 10, 10)
      val rec2 = rec1.copy(lat = 20, lon = 20)
      val rec3 = rec1.copy(sourceId = UUID.randomUUID)

      def insertRecordAndCheckExistence(record: LocationRecord): Unit = {
        repoComp.repository.insertLocationRecord(record)
          .mapResult(_.isSuccessful mustBe true)
        cassandraUtils.getBySourceId(record.sourceId)
          .mapResult(_.head mustBe record)
      }

      insertRecordAndCheckExistence(rec1)
      insertRecordAndCheckExistence(rec2)
      insertRecordAndCheckExistence(rec3)
    }

    "delete records for a source" in {

      val parRecords = generateCoordinateRecordsPar(10, 100)
      val records = parRecords.seq.par

      storeRecords(records)

      val sourceToDelete = records.head._1

      repoComp.repository.deleteLocationsBySourceId(sourceToDelete)
        .mapResult(_.isSuccessful mustBe true)

      cassandraUtils.getBySourceId(sourceToDelete)
        .mapResult(_.isEmpty mustBe true)
    }
  }
}

