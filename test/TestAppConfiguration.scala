import infrastructure.AppConfiguration

trait TestAppConfiguration extends AppConfiguration  {
  override val hosts: List[String] = {

    val deployHosts = getDeployHosts
    if (deployHosts.isEmpty){
      List("127.0.0.1")
    }
    else
      deployHosts
  }

  override val keyspace: String = "LocationTrackerTests"
  override val timeoutDuration: Int = 20
}
